#include "mex.h"
#include <math.h>
#include <strings.h>
#include <string.h>

#define max(x,y) (((x)<=(y))?(y):(x))
#define min(x,y) (((x)>=(y))?(y):(x))



/* ---------------------------------------------------------------- */
double
product(mxArray const * X_array,
        double const * w,
        mwIndex i)
{
  double const * X = mxGetPr(X_array) ;
  mwIndex const * Jc = mxGetJc(X_array) ;
  mwIndex const * Ir = mxGetIr(X_array) ;
  double acc = 0 ;
  if (!mxIsSparse(X_array)) {
    mexErrMsgTxt("X is not sparse") ;
  }
  {
    int index = Jc[i] ;
    int stop = Jc[i+1] ;
    while(index < stop) {
      acc += w[Ir[index]] * X[index] ;
      index++ ;
    }
  }
  return acc ;
}

/* ---------------------------------------------------------------- */
void
accumulate(double * w,
           mxArray const * X_array,
           double const * diagonal,
           double mult,
           mwIndex i)
{
  double const * X = mxGetPr(X_array) ;
  mwIndex const * Jc = mxGetJc(X_array) ;
  mwIndex const * Ir = mxGetIr(X_array) ;
  if (!mxIsSparse(X_array)) {
    mexErrMsgTxt("X is not sparse") ;
  }
  {
    int index = Jc[i] ;
    int stop = Jc[i+1] ;
    while(index < stop) {
      int j = Ir[index] ;
      w[j] += X[index] * diagonal[j] * mult ;
      index++ ;
    }
  }
}


/* ---------------------------------------------------------------- */
void
calc_objective(double * en,
               int blockSize,
               double lambda,
               mxArray const * data_array,
               mxArray const * labels_array,
               mxArray const * model_array,
               mxArray const * spmodel_array)
{
  int i ;
  int numPos = 0 ;
  int numNeg = 0 ;
  int numData = mxGetN(data_array) ;
  double const * labels = mxGetPr(labels_array) ;
  double const * model = mxGetPr(model_array) ;
  double const * spmodel = mxGetPr(spmodel_array) ;

  en[0] = 0 ; /* tot */
  en[1] = 0 ; /* reg */
  en[2] = 0 ; /* pos */
  en[3] = 0 ; /* neg */

  /* compute loss */
  for (i = 0 ; i < numData ; ++i) {
    double score = product(data_array, spmodel, i) ;
    if (labels[i] > 0) {
      en[2] += max(0, 1 - score) ;
      numPos ++ ;
    } else {
      en[3] += max(0, 1 + score) ;
      numNeg ++ ;
    }
  }
  en[2] = en[2] / (numPos + numNeg) ;
  en[3] = en[3] / (numPos + numNeg) ;

  /* compute regularizer */
  {
    double acc = 0 ;
    int dimension = mxGetNumberOfElements(model_array) ;
    for (i = 0 ; i < dimension ; ++i) acc += model[i]*model[i] ;
    en[1] = 0.5 * lambda * acc ;
  }
  en[0] = en[1] + en[2] + en[3] ;
}

enum {
  IN_BLOCKSIZE = 0 ,
  IN_DATA,
  IN_LABELS,
  IN_LAMBDA,
  IN_NUMITER,
  IN_PRECOND
} ;

/* ---------------------------------------------------------------- */
void randperm(int * perm, int dimension)
{
  int i, j, k ;
  for (i = 0; i < dimension ; i++) perm[i] = i;
  for (i = 0; i < dimension ; i++) {
    j = (int)(drand48()*(dimension - i)) + i ;
    j = min(j, dimension-1) ;
    k = perm[i] ; perm[i] = perm[j] ; perm[j] = k ;
  }
}

/* ---------------------------------------------------------------- */
void mexFunction (int nout, mxArray * out [],
                  int nin, const mxArray * in [])
{
  mxArray const * blockSize_array = in[IN_BLOCKSIZE] ;
  mxArray const * data_array = in[IN_DATA] ;
  mxArray const * labels_array = in[IN_LABELS] ;
  mxArray const * lambda_array = in[IN_LAMBDA] ;
  mxArray const * numIter_array = in[IN_NUMITER] ;
  mxArray const * precond_array = in[IN_PRECOND] ;
  int i, j ;

  if (nin < 5) {
    mexErrMsgTxt("5 arguments please.") ;
  }

  {
    int dimension = mxGetM(data_array) ;
    int numData = mxGetN(data_array) ;
    int t, pass, ent ;
    int maxNumIterations = mxGetScalar(numIter_array) ;
    mxArray * model_array = mxCreateDoubleMatrix(dimension, 1, mxREAL) ;
    mxArray * spmodel_array = mxCreateDoubleMatrix(dimension, 1, mxREAL) ;
    double * model = mxGetPr(model_array) ;
    double * spmodel = mxGetPr(spmodel_array) ;
    double *  gradient = mxMalloc(dimension * sizeof(double)) ;
    double const * precond = mxGetPr(precond_array) ;
    double const * labels = mxGetPr(labels_array) ;
    double lambda = mxGetScalar(lambda_array) ;
    int * perm = mxMalloc(numData * sizeof(int)) ;
    int * active = mxCalloc(sizeof(int), numData) ;
    int t0 = max(ceil(10 / lambda),1) ;
    double xrate = 0 ;
    double tol = 1.001 ;
    double minen = 1e12 ;
    int blockSize ;
    int converged ;
    int numPos ;
    int numNeg ;
    int numActivePos ;
    int numActiveNeg ;
    int const regulInterval = 30 ;
    int const objectiveInterval = max(1,ceil(maxNumIterations / 50)) ;
    mxArray * objective_array = mxCreateDoubleMatrix(4, maxNumIterations / objectiveInterval + 1, mxREAL) ;
    double * objective = mxGetPr(objective_array) ;

    if (mxGetNumberOfElements(labels_array) != numData) {
      mexErrMsgTxt("LABELS does not match the data dimension.") ;
    }
    if (mxGetClassID(labels_array) != mxDOUBLE_CLASS) {
      mexErrMsgTxt("LABELS is not of class DOUBLE.") ;
    }
    if (mxGetNumberOfElements(precond_array) != dimension) {
      mexErrMsgTxt("PRECOND does not match the data dimension.") ;
    }
    if (mxGetClassID(precond_array) != mxDOUBLE_CLASS) {
      mexErrMsgTxt("PRECOND is not of class DOUBLE.") ;
    }
    if (mxGetClassID(blockSize_array) != mxDOUBLE_CLASS ||
        mxGetNumberOfElements(blockSize_array) != 1) {
      mexErrMsgTxt("BLOCKSIZE is not a scalar") ;
    }
    blockSize = mxGetScalar(blockSize_array) ;

    mexPrintf("mbpegasos: maxNumIterations = %d\n", maxNumIterations) ;
    mexPrintf("mbpegasos: dimension = %d\n", dimension) ;
    mexPrintf("mbpegasos: numData = %d\n", numData) ;
    mexPrintf("mbpegasos: lambda = %f\n", lambda) ;
    mexPrintf("mbpegasos: t0 = %f\n", t0) ;
    mexPrintf("mbpegasos: regulInterval = %d\n", regulInterval) ;
    mexPrintf("mbpegasos: objectiveInterval = %d\n", objectiveInterval) ;

    memset(gradient, 0, sizeof(double)*dimension) ;
    for (j = 0 ; j < numData ; ++j) active[j] = -10 ;

    pass = 0 ;
    t = 0 ;
    ent = 0 ;
    xrate = 0 ;
    converged = 0 ;
    for (; t < maxNumIterations && !converged; ++pass) {
      /*mexPrintf("mbpegasos: pass %d\n", pass+1) ;*/
      randperm(perm, numData) ;

      for (i = 0 ; i < numData && t < maxNumIterations; ++i, ++t) {
        int label ;
        int* act = active + perm[i] ;
        double score, rate = 1 / (lambda * (t + t0)) ;
        xrate += rate ;

        /* accumulate mini batch */
        if (*act <= 0) {
          score = product(data_array, spmodel, perm[i]) ;
          label = labels[perm[i]] ;
          if (score*label < 1) {
            accumulate(gradient, data_array, precond, rate * label, perm[i]) ;
            -- active[perm[i]] ;
          } else {
            if (*act == 0) *act += (rand() & 0x7) ;
            *act += 1 ;
          }
        } else {
          *act -= 1 ;
        }

        /* check objective */
        if (t % objectiveInterval == 0) {
          calc_objective(objective + 4 * ent,
                         blockSize,
                         lambda,
                         data_array,
                         labels_array,
                         model_array,
                         spmodel_array) ;
          numPos =0 ;
          numNeg = 0 ;
          numActivePos = 0 ;
          numActiveNeg = 0 ;
          for (j = 0 ; j < numData ; ++j) {
            if (labels[j] >= 0) {
              numPos++ ;
              if (active[j] <=0) numActivePos ++ ;
            } else {
              numNeg++ ;
              if (active[j] <=0) numActiveNeg ++ ;
            }
          }
          mexPrintf("mbpegasos: %05d: t:%8.4g r:%8.4g p:%8.4g n:%8.4g; iter: %d act: %d/%d %d/%d\n",
                    ent, objective[4*ent],objective[4*ent+1],
                    objective[4*ent+2],objective[4*ent+3],t,
                    numActivePos,numPos,
                    numActiveNeg,numNeg) ;
          minen = (minen < objective[4*ent]) ? minen : objective[4*ent];
          if (ent > 4 &&
              objective[4*(ent-3)] / minen <= tol &&
              objective[4*(ent-2)] / minen <= tol &&
              objective[4*(ent-1)] / minen <= tol) {
            mexPrintf("mbpegasos: %05d: CONVERGED (%d iterations)\n", ent, t) ;
            converged = 1 ;
            break ;
          }
          ++ ent ;
        }

        /* gradient step */
        if (t % regulInterval == 0) {
          double block = 0 ;

          /* multiply gradient by inv(D) */
          for (j = dimension-1 ; j >= 0 ; --j) {
            block += gradient[j] ;
            gradient[j] = block ;
            if (j % blockSize == 0) block = 0 ;
          }

          /* do step */
          for (j = 0 ; j < dimension ; ++j) {
            model[j] = model[j] - xrate * lambda * precond[j] * model[j] + gradient[j] ;
          }
          xrate = 0 ;
          memset(gradient, 0, sizeof(double)*dimension) ;

          /* spmodel = inv(D)' * model */
          block = 0 ;
          for (j = 0 ; j < dimension ; ++j) {
            if (j % blockSize == 0) block = 0 ;
            block += model[j] ;
            spmodel[j] = block ;
          }
        }
      }
    }

    out[0] = model_array ;
    out[1] = spmodel_array ;
    out[2] = objective_array ;
    mxFree(gradient) ;
  }
}
