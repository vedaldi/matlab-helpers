rand('state',0) ;
x = rand(2,100) ;
k = 3 ;
gmm = gmmtrain(x,k,'verbose',true,...
               'minvariancerel',0,...
               'minvarianceabs',0,...
               'numrepetitions',2) ;
assign = gmmtest(gmm, x) ;

% compare to MATLAB native version
if 0
  s.mu = gmm.mu' ;
  s.Sigma = shiftdim(gmm.sigma2,-1) ;
  s.PComponents = gmm.prior ;
  gmm_ = gmdistribution.fit(x', k, 'covtype', 'diagonal', 'start', s) ;
end