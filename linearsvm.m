function [w, info, state] = linearsvm(x, y, H, varargin)
% SVM Solve linear SVM

  opts.maxNumIterations = 100 ;
  opts.compressed = 0 ;
  opts.verbose = false ;
  opts.model = [] ;
  opts.loss = 'binary' ;
  opts.regulIsInverse = false ;
  opts.relTol = 1.005 ;
  opts = vl_argparse(opts, varargin) ;

  % pos neg
  selPos = find(y > 0) ;
  selNeg = find(y < 0) ;
  numPos = numel(selPos) ;
  numNeg = numel(selNeg) ;
  if opts.compressed
    dimension = opts.compressed ;
  else
    dimension = size(x,1) ;
  end
  y = y(:)' ;

  % set an initial model if any
  if isempty(opts.model)
    w = zeros(dimension, 1) ;
    regul0 = 0 ;
  else
    w = opts.model ;
    if ~opts.regulIsInverse
      regul0 = state.lambda * w' * (H * w) / 2 ;
    else
      regul0 = state.lambda * w' * (H \ w) / 2 ;
    end
  end

  % ------------------------------------------------------------------
  %                                           Cutting plane iterations
  % ------------------------------------------------------------------
  info.objective = zeros(5, opts.maxNumIterations) ;
  info.elapsed = tic ;
  state = bundler2() ;
  state.w = w ;

  for t = 1:opts.maxNumIterations
    switch opts.loss
      case 'binary'
        [a,b,loss,hardLoss] = binary_loss(opts, w, x, y) ;
      case 'roc_auc'
        [a,b,loss,hardLoss] = roc_auc_loss(opts, w, x, y) ;
    end
    if t == 1
      regul = regul0 ;
    else
      regul = state.regularizer ;
    end
    e(1) = regul + loss ;
    e(2) = NaN ;
    e(3) = regul ;
    e(4) = loss ;
    e(5) = hardLoss ;
    info.objective(:,t) = e ;

    fprintf('%s: iter %d: loss:%2.4f, regul:%2.4f, tot:%2.4f\n', ...
            mfilename, t, ...
            info.objective(4,t), ...
            info.objective(3,t), ...
            info.objective(1,t)) ;

    % check termination conditions
    if t > 1
      obj1 = info.objective(2,t-1) ;
      obj2 = info.objective(1,t) ;
      gap = obj2 - obj1 ;
      if gap < -1e-8
        fprintf('%s: iter %d: something wrong with constraint generation\n', ...
                mfilename, t) ;
      end
      % max with prev. slack
      fprintf('%s: iter %d: partial: %f, full: %f, gap: %f\n', ...
              mfilename, t, obj1, obj2, gap) ;
      if obj2 / (obj1 + 1e-12) < opts.relTol
        fprintf('%s: iter %d: CONVERGED\n', mfilename, t) ;
        break ;
      end
    end

    % bunlder step
    if ~opts.regulIsInverse
      iHa = H \ a ;
    else
      iHa = H * a ;
    end
    state = bundler2(state, a, iHa, b) ;
    w = state.w ;
    info.objective(2,t) = state.dualObjective ;
  end
  info.elapsed = toc(info.elapsed) ;
  info.objective = info.objective(:,1:t) ;
  if opts.verbose
    figure(1) ; clf ;
    semilogy(1:t,info.objective(:,1:t)','.-') ;
    legend('obj','dual', 'regul', 'loss', 'hloss') ;
    grid on ;
    xlabel('iteration') ;
    title(sprintf('loss:%s', opts.loss)) ;
    drawnow ;
  end
end

% --------------------------------------------------------------------
function [a,b,loss,hardLoss] = binary_loss(opts, w, x, y)
% --------------------------------------------------------------------
  num = numel(y) ;
  if opts.compressed
    score = vl_cmultiply(w, x) ;
  else
    score = full(w' * x) ;
  end

  margin = y .* score ;
  loss = sum(max(1 - margin, 0)) / num ;
  hardLoss = sum(margin < 0) / num ;
  weight = (margin < 1)  / num ;
  b = sum(weight) ;

  if opts.compressed
    dimension = numel(w) ;
    a = zeros(dimension,1) ;
    a = vl_caccumulate(a, x, y .* weight) ;
  else
    a = double(full(x * (y .* weight)')) ;
  end
end

% --------------------------------------------------------------------
function [a,b,loss,hardLoss] = roc_auc_loss(opts, w, x, y)
% --------------------------------------------------------------------
  selPos = find(y > 0) ;
  selNeg = find(y < 0) ;
  numPos = numel(selPos) ;
  numNeg = numel(selNeg) ;
  numPairs = numPos * numNeg ;

  if opts.compressed
    score = vl_cmultiply(w, x) ;
  else
    score = full(w' * x) ;
  end

  % compute the area under the ROC curve
  % this is the number of incorrectly ranked pairs
  [drop, perm] = sort(score) ;
  cumPos = cumsum(y(perm) > 0) ;
  hardLoss = sum(cumPos(y(perm) < 0)) / numPairs ;

  % compute the convex UB on the loss
  % we use a margin of 1
  score(selPos) = score(selPos) - 1 ;
  [drop, perm] = sort(score) ;
  weightNeg(perm) = cumsum(y(perm) > 0) ;
  weightPos(perm) = numNeg - cumsum(y(perm) < 0) ;
  weight(selPos) = + weightPos(selPos) / numPairs ;
  weight(selNeg) = - weightNeg(selNeg) / numPairs ;
  loss = - score * weight' ;

  if opts.compressed
    a = zeros(dimension,1) ;
    a = vl_caccumulate(a, x, weight) ;
  else
    a = double(full(x * weight')) ;
  end
  b = sum(weight(selPos)) ;
end

