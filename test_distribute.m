function test_distribute

[a,b]=distribute(@two, 1:10)
[a,b]=distribute(@two, 1:10, 'subset', [2 5])

distribute profile on ;
[a,b]=distribute(@two, 1:10, 'subset', [2 5]) ;
distribute profile status ;
mpiprofile('viewer', distribute('profile', 'get')') ;

distribute profile off ;
distribute profile status ;

function [a,b]=two(x)
a=2*x;
b=3*x;
