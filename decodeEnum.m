function name = decodeEnum(enum, value)
% DECODEENUM
%
%  NAME = DECODEENUM(ENUM, VALUE)
%
%  Author:: Andrea Vedaldi

names = fieldnames(enum)' ;

for name = names
  name = char(name) ;
  thisValue = enum.(name) ;
  ok = false ;
  if isnumeric(thisValue)
    ok = isequalwithequalnans(double(thisValue), double(value)) ;
  else
    ok = isequa(thisValue, value) ;
  end
  if ok, return ; end
end

% not found !
name = [] ;
