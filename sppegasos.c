#include "mex.h"
#include <math.h>
#include <strings.h>
#include <string.h>

#define max(x,y) (((x)<=(y))?(y):(x))
#define min(x,y) (((x)>=(y))?(y):(x))



/* ---------------------------------------------------------------- */
double
product(mxArray const * X_array,
        double const * w,
        mwIndex i)
{
  double const * X = mxGetPr(X_array) ;
  mwIndex const * Jc = mxGetJc(X_array) ;
  mwIndex const * Ir = mxGetIr(X_array) ;
  double acc = 0 ;
  if (!mxIsSparse(X_array)) {
    mexErrMsgTxt("X is not sparse") ;
  }
  {
    int index = Jc[i] ;
    int stop = Jc[i+1] ;
    while(index < stop) {
      acc += w[Ir[index]] * X[index] ;
      index++ ;
    }
  }
  return acc ;
}

/* ---------------------------------------------------------------- */

void
accumulate(double * w,
           mxArray const * X_array,
           double const * diagonal,
           double mult,
           mwIndex i)
{
  double const * X = mxGetPr(X_array) ;
  mwIndex const * Jc = mxGetJc(X_array) ;
  mwIndex const * Ir = mxGetIr(X_array) ;
  if (!mxIsSparse(X_array)) {
    mexErrMsgTxt("X is not sparse") ;
  }
  {
    int index = Jc[i] ;
    int stop = Jc[i+1] ;
    while(index < stop) {
      int j = Ir[index] ;
      w[j] += X[index] * diagonal[j] * mult ;
      index++ ;
    }
  }
}


/* ---------------------------------------------------------------- */
void
calc_objective(double * en,
               mxArray const * data_array, /* dense */
               mxArray const * labels_array,
               mxArray const * model_array,
               mxArray const * Hmodel_array)
{
  int i ;
  int numPos = 0 ;
  int numNeg = 0 ;
  int numData = mxGetN(data_array) ;
  double const * labels = mxGetPr(labels_array) ;
  double const * model = mxGetPr(model_array) ;
  double const * Hmodel = mxGetPr(Hmodel_array) ;

  en[0] = 0 ; /* tot */
  en[1] = 0 ; /* reg */
  en[2] = 0 ; /* pos */
  en[3] = 0 ; /* neg */

  /* compute loss */
  for (i = 0 ; i < numData ; ++i) {
    double score = product(data_array, model, i) ;
    if (labels[i] > 0) {
      en[2] += max(0, 1 - score) ;
      numPos ++ ;
    } else {
      en[3] += max(0, 1 + score) ;
      numNeg ++ ;
    }
  }
  en[2] = en[2] / (numPos + numNeg) ;
  en[3] = en[3] / (numPos + numNeg) ;

  /* compute regularizer */
  {
    double acc = 0 ;
    int dimension = mxGetNumberOfElements(model_array) ;
    for (i = 0 ; i < dimension ; ++i) acc += model[i] * Hmodel[i] ;
    en[1] = 0.5 * acc ;
  }
  en[0] = en[1] + en[2] + en[3] ;
}

enum {
  IN_H = 0 ,
  IN_DATA,
  IN_LABELS,
  IN_LAMBDA,
  IN_NUMITER,
  IN_PRECOND
} ;

/* ---------------------------------------------------------------- */
void randperm(int * perm, int dimension)
{
  int i, j, k ;
  for (i = 0; i < dimension ; i++) perm[i] = i;
  for (i = 0; i < dimension ; i++) {
    j = (int)(drand48()*(dimension - i)) + i ;
    j = min(j, dimension-1) ;
    k = perm[i] ; perm[i] = perm[j] ; perm[j] = k ;
  }
}

/* ---------------------------------------------------------------- */
void mexFunction (int nout, mxArray * out [],
                  int nin, const mxArray * in [])
{
  mxArray const * H_array = in[IN_H] ;
  mxArray const * data_array = in[IN_DATA] ;
  mxArray const * labels_array = in[IN_LABELS] ;
  mxArray const * lambda_array = in[IN_LAMBDA] ;
  mxArray const * numIter_array = in[IN_NUMITER] ;
  mxArray const * precond_array = in[IN_PRECOND] ;
  int i, k, j ;

  if (nin < 5) {
    mexErrMsgTxt("5 arguments please.") ;
  }
  if (!mxIsCell(H_array)) {
    mexErrMsgTxt("H is not a cell.") ;
  }

  {
    int dimension = mxGetM(data_array) ;
    int numData = mxGetN(data_array) ;
    int t, pass, ent ;
    int maxNumIterations = mxGetScalar(numIter_array) ;
    mxArray * model_array = mxCreateDoubleMatrix(dimension, 1, mxREAL) ;
    mxArray * Hmodel_array = mxCreateDoubleMatrix(dimension, 1, mxREAL) ;
    double * model = mxGetPr(model_array) ;
    double * Hmodel= mxGetPr(Hmodel_array) ;
    double const * precond = mxGetPr(precond_array) ;
    double const * labels = mxGetPr(labels_array) ;
    double * tmp = mxMalloc(dimension * sizeof(double)) ;
    double lambda = mxGetScalar(lambda_array) ;
    int * perm = mxMalloc(numData * sizeof(int)) ;
    int t0 = max(ceil(10 / lambda),1) ;
    double xrate = 0 ;
    int const regulInterval = 10 ;
    int const objectiveInterval = max(1,ceil(maxNumIterations / 50)) ;
    mxArray * objective_array = mxCreateDoubleMatrix(4, maxNumIterations / objectiveInterval + 1, mxREAL) ;
    double * objective = mxGetPr(objective_array) ;

    if (mxGetNumberOfElements(labels_array) != numData) {
      mexErrMsgTxt("LABELS does not match the data dimension.") ;
    }
    if (mxGetNumberOfElements(precond_array) != dimension) {
      mexErrMsgTxt("PRECOND does not match the data dimension.") ;
    }
    if (mxGetClassID(precond_array) != mxDOUBLE_CLASS) {
      mexErrMsgTxt("PRECOND is not of class DOUBLE.") ;
    }
    if (mxGetClassID(labels_array) != mxDOUBLE_CLASS) {
      mexErrMsgTxt("LABELS is not of class DOUBLE.") ;
    }

    /*    if (mxGetNumberOfElements(labels_array) != 1) {
      mexErrMsgTxt("LABELS has not the right size.") ;
      }*/

    mexPrintf("spegasos: maxNumIterations = %d\n", maxNumIterations) ;
    mexPrintf("spegasos: dimension = %d\n", dimension) ;
    mexPrintf("spegasos: numData = %d\n", numData) ;
    mexPrintf("spegasos: lambda = %f\n", lambda) ;
    mexPrintf("spegasos: t0 = %f\n", t0) ;
    mexPrintf("spegasos: regulInterval = %d\n", regulInterval) ;
    mexPrintf("spegasos: objectiveInterval = %d\n", objectiveInterval) ;

    pass = 0 ;
    t = 0 ;
    ent = 0 ;
    xrate = 0 ;
    for (; t < maxNumIterations; ++pass) {
      /*mexPrintf("spegasos: pass %d\n", pass+1) ;*/
      randperm(perm, numData) ;

      for (i = 0 ; i < numData && t < maxNumIterations; ++i, ++t) {
        int label ;
        double score, rate = 1 / (lambda * (t + t0)) ;
        xrate += rate ;

        /* subgradient step */
        score = product(data_array, model, perm[i]) ;
        label = labels[perm[i]] ;
        if (score*label < 1) {
          accumulate(model, data_array, precond, rate * label, perm[i]) ;
        }

        /* compute the product H * model */
        if (t % regulInterval == 0 || t % objectiveInterval == 0) {
          memcpy(Hmodel, model, sizeof(double)*dimension) ;
          for (k = 0 ; k < mxGetNumberOfElements(H_array) ; ++k) {
            mxArray const * factor_array = mxGetCell(H_array, k) ;
            if (!mxIsSparse(factor_array)) {
              mexErrMsgTxt("H contains a non-sparse martrix.") ;
            }
            if (mxGetM(factor_array) != dimension ||
                mxGetN(factor_array) != dimension) {
              mexErrMsgTxt("H constains a factor of incorrect size.") ;
            }
            memcpy(tmp, Hmodel, sizeof(double)*dimension) ;
            for (j = 0 ; j < dimension ; ++j) {
              Hmodel[j] = product(factor_array, tmp, j) ;
            }
          }
        }

        /* check objective */
        if (t % objectiveInterval == 0) {
          calc_objective(objective + 4 * (ent++),
                         data_array,
                         labels_array,
                         model_array,
                         Hmodel_array) ;
          mexPrintf("spegasos: %05d: t/r/p/n %5.2g %5.2g %5.2g %5.2g; iter: %d\n",
                    ent, objective[0],objective[1],
                    objective[2],objective[3],t) ;
        }

        /* regularizer step */
        if (t % regulInterval == 0) {
          int j ;
          for (j = 0 ; j < dimension ; ++j) {
            model[j] -= Hmodel[j] * xrate ;
          }
          xrate = 0 ;
        }

      }
    }

    out[0] = model_array ;
    out[1] = objective_array ;
    mxDestroyArray(Hmodel_array) ;
    mxFree(tmp) ;
  }
}
