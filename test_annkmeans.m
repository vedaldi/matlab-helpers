% TEST_ANNKMEANS

switch 1
  case 1
    % small scale test
    X = single(randn(128,50000)) ;
    K = 100 ;

    elapsed_serial = tic ;
    [centers, tree, en] = annkmeans(X, K, 'verbose', true) ;
    elapsed_serial = toc(elapsed_serial) ;

    matlabpool 3 ;
    elapsed_parallel = tic ;
    [centers_, tree_, en_] = annkmeans(X, K, 'verbose', true) ;
    elapsed_parallel = toc(elapsed_parallel) ;
    matlabpool close ;

    elapsed_serial
    elapsed_parallel

    max(max(abs(centers-centers_))) / max(max(centers)) * 100
    max(max(abs(en-en_))) / max(max(en)) * 100

  case 2
    % large scale test
    K = 50e3 ;
    X = single(randn(128,50e4)) ;

    if matlabpool('size') < 2, matlabpool 40 ; end
    elapsed_parallel = tic ;
    [centers_, tree_, en_] = annkmeans(X, K, 'verbose', true) ;
    elapsed_parallel = toc(elapsed_parallel) ;
    matlabpool close ;

    elapsed_parallel
end


