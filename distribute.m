function varargout = distribute(func, range, varargin)
% DISTRIBUTE  Distribute a function on the cluster
%  RESULTS = DISTRIBUTE(FUNC, RANGE) is equivalent to
%
%   for i = 1:length(range), RESULTS(i) = FUNC(RANGE(i)), end
%
%  but executes FUNC using the avialable labs. Differently from
%  PARFOR, DISTRIBUTE uses always the same nodes for the same
%  elements of range.
%
%  DISTRIBUTE PROFILE ON switches on profiling. DISTRIBUTE PROFILE OFF
%  switches it off. INFO = DISTRIBUTE('PROFILE','GET') returns the
%  profiling information that can be viewed with MPIPROFILE('viewer',
%  INFO). The latter can be obtained atuomatically by DISTRIBUTE
%  PROFILE VIEWER as well.

% Author: Andre Vedaldi

opts.mapOnRange = false ;
opts.batchSize = +inf ;
opts.subset = NaN ;
opts.args = {} ;
opts.maxNumLabs = +inf ;
opts = vl_argparse(opts, varargin) ;

% persistent variables are used to collect profiling information
persistent profileInfo profileStatus ;
if isempty(profileStatus), profileStatus = false ; end
if isnumeric(profileInfo), profileInfo = {} ; end

% check for special inputs
if isstr(func)
  switch lower(func)
    case 'profile'
      switch lower(range)
        case 'on', profileStatus = true ;
        case 'off', profileStatus = false ; profileInfo = {} ;
        case 'get', varargout{1} = [profileInfo{:}] ;
        case 'status'
          fprintf('%s: profile on = %d, profile info items = %d\n', ...
                  mfilename, profileStatus, numel(profileInfo)) ;
        case {'viewer', 'view'}
          mpiprofile('viewer', distribute('profile','get')) ;
        otherwise
          error('Unknown command ''%s''.', range) ;
      end
      return ;
    otherwise
      error('Unknown command ''%s''.', func) ;
  end
end
if isnan(opts.subset)
  subset = range ;
else
  subset = opts.subset ;
end

% Make a wrapper to call func with the additional optional args. This
% must be done in another function makeCallee() to control what goes
% into the nested workspace of callee (major bottleneck otherwise).
callee = makeCallee(func, opts.args) ;

% Get the number of available labs; if there are none, set this to one
% for local computation.
n = max(min(matlabpool('size'), opts.maxNumLabs), 1) ;

% assing range elements to nodes
if opts.mapOnRange
  assignments = mod(range - 1, n) + 1 ;
else
  assignments = mod((1:length(range)) - 1, n) + 1 ;
end

if ~isa(func, 'function_handle')
  % compute independent batches and return them
  batches = makeBatches(range, assignments, subset, n , opts.batchSize) ;
  for b = 1:length(batches), batches{b} = [batches{b}{:}]; end
  varargout{1} = batches ;
  return ;
end

% if the subset to operate on is empty, return an empty result
numOutputs = nargout ;
for i = 1:numOutputs
  varargout{i} = cell(1,numel(subset)) ;
end
if isempty(subset), return ; end

% if there is no matlabpool, do locally
if matlabpool('size') == 0
  [varargout{1:numOutputs}] = arrayfun(callee, subset, 'UniformOutput', false)  ;
  return ;
end

% compute workload for each lab
batches = makeBatches(range, assignments, subset, n, opts.batchSize) ;
labElapsed = zeros(1,n) ;
labWorkload = zeros(1,n) ;

start = tic ;

% compute parallel batches
for b = 1:length(batches)
  fprintf('%s: processing batch %d of %d on %d labs\n', mfilename, b, length(batches), n) ;
  % by using only a single RESULTS composite the speed is significantly increased
  results = do_broadcast(callee, batches{b}, n, numOutputs, profileStatus) ;
  for lab = 1:n
    labResults = results{lab} ;
    labElapsed(lab) = labElapsed(lab) + labResults{1} ;
    labWorkload(lab) = labWorkload(lab) + numel(batches{b}{lab}) ;
    for i = 1:numOutputs
      [drop, where] = ismember(batches{b}{lab}, subset) ;
      varargout{i}(where) = labResults{i + 2} ;
    end
    if profileStatus, profileInfo{end+1} = labResults{2} ; end
    clear labResults ;
  end
  clear results ;
end

% statistics
spmd (n), [drop,hostnames]=system('hostname') ; end
hostnames = {hostnames{:}} ;
clear drop ;

fprintf('%s: elapsed: %.1f, args size: %.1f MB, statistics [lab host elaps #jobs]:', ...
        mfilename, toc(start), argsSize(opts.args) / 1024^2) ;
[drop,perm_] = sort(hostnames) ;
perm = cell(ceil(n/4),4) ;
perm(1:n) = num2cell(perm_) ;
perm = perm' ;

for i = 1:n
  if mod(i - 1, 4) == 0, fprintf('\n') ; end
  lab = perm{i} ;
  if isempty(lab), fprintf('|') ; continue ; end
  fprintf('|%22s ', sprintf('%2d %8s %5.1f %3d', lab, strtrim(hostnames{lab}), ...
                          labElapsed(lab), labWorkload(lab))) ;
end
fprintf('\n') ;

% --------------------------------------------------------------------
function batches = makeBatches(range, assignments, subset, numLabs, batchSize)
% --------------------------------------------------------------------

numBatches = 1 ;
for lab = 1:numLabs
  labSubset{lab} = intersect(range(assignments == lab), subset) ;
end

batchSize = min(batchSize, max(cellfun(@numel,labSubset))) ;
numBatches = ceil(max(cellfun(@numel,labSubset) / max(batchSize,1))) ;

batches = cell(1,numBatches) ;
for b = 1:numBatches
  for lab = 1:numLabs
    sel = [batchSize * (b - 1) + 1 : min(batchSize * b, numel(labSubset{lab}))] ;
    batches{b}{lab} = labSubset{lab}(sel) ;
  end
end

% --------------------------------------------------------------------
function results = do(callee, batches, n, numOutputs, profileStatus)
% --------------------------------------------------------------------
spmd (n)
  if profileStatus
    mpiprofile off ;
    mpiprofile on -detail builtin ;
  end
  results = cell(1, numOutputs + 2) ;
  results{1} = tic ;
  [results{3:numOutputs+2}] = arrayfun(callee, batches{labindex}, 'UniformOutput', false)  ;
  results{1} = toc(results{1}) ;
  if profileStatus
    results{2} = mpiprofile('info') ;
    mpiprofile reset ;
  end ;
end

% --------------------------------------------------------------------
function results = do_broadcast(callee_, batches, n, numOutputs, profileStatus)
% --------------------------------------------------------------------
data = Composite(n) ;
data{1} = callee_ ;

spmd (n)
  if labindex == 1
    callee = labBroadcast(1, data) ;
  else
    callee = labBroadcast(1) ;
  end

  if profileStatus
    mpiprofile off ;
    mpiprofile on -detail builtin ;
  end
  results = cell(1, numOutputs + 2) ;
  results{1} = tic ;
  [results{3:numOutputs+2}] = arrayfun(callee, batches{labindex}, 'UniformOutput', false)  ;
  results{1} = toc(results{1}) ;
  if profileStatus
    results{2} = mpiprofile('info') ;
    mpiprofile reset ;
  end ;
end

% --------------------------------------------------------------------
function callee = makeCallee(func, args)
% --------------------------------------------------------------------
callee = @(r) func(r, args{:}) ;

% --------------------------------------------------------------------
function size = argsSize(args)
% --------------------------------------------------------------------
info = whos('args') ;
size = info.bytes ;

