function n = soaNumEl(a)
% SOANUMEL  Get number of elements in a structure-of-array
%   N = SOANUMEL(A)

names = fieldnames(a) ;
n = size(a.(names{1}), 2) ;
