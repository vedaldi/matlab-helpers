
d = 2 ;
m = 10;
n = 200 ;
x = rand(d, n) ;
w0 = randn(d,1) ;
y = sign(w0'*x+randn(1,n)) ;
dx  = 1 / m ;

% mb encoding
psi = {} ;
for j = 1:n
  for i = 1:d
    k = floor(x(i,j) / dx) ;
    a = x(i,j) - k * dx ;
    e = zeros(m,1) ;
    e(1:k) = 1 ;
    if (k < m), e(k+1) = a ; end
    psi{i,j} = e ;
  end
end
psi = sqrt(dx) * cell2mat(psi) ;

K = vl_alldist(x,'kl1') ;
K_ = psi'*psi ;
figure(1); clf ; imagesc([K K_ K-K_]);

% sparsity
D = eye(m) ;
[j,i]=meshgrid(1:m) ;
D(i==j-1) = -1 ;
D0=D ;
D = eye(m*d) ;
for i=1:d
  s = (1:m) + m *(i-1);
  D(s,s) = D0 ;
end
iD = inv(D) ;
spsi = D * psi ;

% add bias
B = 1 ;

if B
  spsi(end+1,:) = B;
  psi(end+1,:) = B;
end

% learn SVM
lambda = .1 ;
numIters = 100000 ;
prec = ones(m*d, 1) ;
if B
  prec(end+1) = 1/B ;
end
[w, spw, en] = mbpegasos(m, sparse(spsi), y, lambda, numIters, prec) ;

s = 1:m*d ;

D' * spw(s) - w(s)
spw(s)' * spsi(s,:) - w(s)' * psi(s,:)

% learn with linear svm
np = sum(y>0);
nn = sum(y<0);
[w_,info] = linearsvm(psi, y, lambda, 'verbose', 0);
%info.objective = info.objective * np/n ;

figure(1) ; clf ;
subplot(2,2,1) ; semilogy(en') ; grid on ; ylim([0.01 10]) ;
subplot(2,2,2) ; semilogy(info.objective') ; grid on ;  ylim([0.01 10]) ;
subplot(2,2,3) ; plot([w, w_]); legend('pega', 'cut') ;
