function imdb = imdbFromVoc(vocPath, vocEdition, varargin)
% IMDBFROMVOC  Compute an image database from the VOC dataset
%   IMDB = IMDBFROMVOC(VOCPATH, VOCEDITION)
%
%   Example::
%     To generate a database IMDB for VOC 2010:
%
%       imdb = imdbFromVoc('/path/to/VOCDevkit_2010', '2010')

  opts.idOffset = 0 ;
  opts = vl_argparse(opts, varargin) ;

  imdb.images.id = [] ;
  imdb.images.set = uint8([]) ;
  imdb.images.name = {} ;
  imdb.images.size = zeros(2,0) ;
  %imdb.dir = fullfile(vocPath, ['VOC' vocEdition], 'JPEGImages') ;
  imdb.path.image = fullfile(vocPath, ['VOC' vocEdition], 'JPEGImages', '%s.jpg') ;
  imdb.path.objectSegmentation = fullfile(vocPath, ['VOC' vocEdition], 'SegmentationObject', '%s.png') ;
  imdb.path.classSegmentation = fullfile(vocPath, ['VOC' vocEdition], 'SegmentationClass', '%s.png') ;
  imdb.classes.name = {'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car', ...
                       'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', ...
                       'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor'} ;
  imdb.classes.imageIds = cell(1,20) ;
  imdb.sets.TRAIN07 = uint8(1) ;
  imdb.sets.VAL07   = uint8(2) ;
  imdb.sets.TEST07  = uint8(3) ;
  imdb.sets.TRAIN08 = uint8(4) ;
  imdb.sets.VAL08   = uint8(5) ;
  imdb.sets.TEST08  = uint8(6) ;
  imdb.sets.TRAIN09 = uint8(7) ;
  imdb.sets.VAL09   = uint8(8) ;
  imdb.sets.TEST09  = uint8(9) ;
  imdb.sets.TRAIN10 = uint8(10) ;
  imdb.sets.VAL10   = uint8(11) ;
  imdb.sets.TEST10  = uint8(12) ;
  imdb.sets.TRAIN11 = uint8(13) ;
  imdb.sets.VAL11   = uint8(14) ;
  imdb.sets.TEST11  = uint8(15) ;
  imdb.sets.TRAIN12 = uint8(16) ;
  imdb.sets.VAL12   = uint8(17) ;
  imdb.sets.TEST12  = uint8(18) ;

  function add(setName, setCode)
    j = length(imdb.images.id) ;
    segAnnoPath = fullfile(vocPath, ['VOC' vocEdition], ...
                           'ImageSets', 'Segmentation', ...
                           [setName '.txt']) ;
    if ~exist(segAnnoPath)
      warning('skipping set %s as it appears to be unavailable', setName) ;
      return ;
    end
    fprintf('%s: reading %s\n', mfilename, segAnnoPath) ;
    segNames = textread(segAnnoPath, '%s') ;
    for i=1:length(segNames), segMap(segNames{i}) = i ; end

    for ci = 1:length(imdb.classes.name)
      className = imdb.classes.name{ci} ;
      annoPath = fullfile(vocPath, ['VOC' vocEdition], ...
                          'ImageSets', 'Main', ...
                          [className '_' setName '.txt']) ;
      fprintf('%s: reading %s\n', mfilename, annoPath) ;
      [names,labels] = textread(annoPath, '%s %f') ;
      for i=1:length(names)
        if ~map.isKey(names{i})
          j = j + 1 ;
          map(names{i}) = j ;
          imdb.images.id(j) = j + opts.idOffset ;
          imdb.images.set(j) = setCode ;
          imdb.images.name{j} = names{i} ;
          imdb.images.segmentation(j) = segMap.isKey(names{i}) ;
          %info = imfinfo(fullfile(imdb.dir, imdb.images.name{j}))
          info = imfinfo(sprintf(imdb.path.image,imdb.images.name{j})) ;
          imdb.images.size(:,j) = [info.Width ; info.Height] ;
          fprintf('\radded %s', imdb.images.name{j}) ;
        else
          j = map(names{i}) ;
        end
        if labels(i) > 0, imdb.classes.imageIds{ci}(end+1) = j ; end
      end
    end
  end

  map = containers.Map() ;
  segMap = containers.Map() ;
  suffix = vocEdition(end-1:end) ;
  add('train', imdb.sets.(['TRAIN' suffix])) ;
  add('val', imdb.sets.(['VAL' suffix])) ;
  add('test', imdb.sets.(['TEST' suffix])) ;
end
