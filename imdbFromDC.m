function imdb = imdbFromDC(dcPath, varargin)
% IMDBFROMDC  Compute an image database from the VOC dataset
%   IMDB = IMDBFROMDC(VOCPATH, VOCEDITION)
%
%   Example::
%     To generate a database IMDB for Scene 15
%
%       imdb = imdbFromVoc('/path/to/dc')
%       imdb = imdbFromVoc('/path/to/dc', 'split', 3) % 6 splits

  opts.split = 0 ;
  opts = vl_argparse(opts, varargin) ;

  imdb.images.id = [] ;
  imdb.images.set = uint8([]) ;
  imdb.images.name = {} ;
  imdb.images.size = zeros(2,0) ;
  imdb.dir = dcPath ;
  imdb.classes.name = {'pedestrian', 'background'} ;
  imdb.classes.imageIds = {} ;

  j = 0 ;
  function ids = add(pattern, setId, base)
    ims = dir(fullfile(imdb.dir, pattern, '*.pgm')) ;
    ids = zeros(1,numel(ims)) ;
    for i=1:length(ims)
      j = j + 1 ;
      ids(i) = (base-1)*1e4 + i ;
      imdb.images.id(j) = ids(i) ;
      imdb.images.set(j) = setId ;
      imdb.images.name{j} = fullfile(pattern, ims(i).name) ;
      info = imfinfo(fullfile(imdb.dir, imdb.images.name{j})) ;
      imdb.images.size(:,j) = [info.Width ; info.Height] ;
      fprintf('\radded %s', imdb.images.name{j}) ;
    end
  end

  if opts.split == 0
    imdb.sets.TRAIN1 = uint8(1) ;
    imdb.sets.TRAIN2 = uint8(2) ;
    imdb.sets.TRAIN3 = uint8(3) ;
    imdb.sets.TEST1 = uint8(4) ;
    imdb.sets.TEST2 = uint8(5) ;
    ids1  = add('1/ped_examples',      imdb.sets.TRAIN1, 1) ;
    ids2  = add('1/non-ped_examples',  imdb.sets.TRAIN1, 2) ;
    ids3  = add('2/ped_examples',      imdb.sets.TRAIN2, 3) ;
    ids4  = add('2/non-ped_examples',  imdb.sets.TRAIN2, 4) ;
    ids5  = add('3/ped_examples',      imdb.sets.TRAIN3, 5) ;
    ids6  = add('3/non-ped_examples',  imdb.sets.TRAIN3, 6) ;
    ids7  = add('T1/ped_examples',     imdb.sets.TEST1, 7) ;
    ids8  = add('T1/non-ped_examples', imdb.sets.TEST1, 8) ;
    ids9  = add('T2/ped_examples',     imdb.sets.TEST2, 9) ;
    ids10 = add('T2/non-ped_examples', imdb.sets.TEST2, 10) ;
    imdb.classes.imageIds{1} = cat(2, ids1, ids3, ids5, ids7, ids9) ;
    imdb.classes.imageIds{2} = cat(2, ids2, ids4, ids6, ids8, ids10) ;
  else
    imdb.sets.TRAIN = uint8(1) ;
    imdb.sets.TEST = uint8(2) ;

    switch mod(opts.split-1, 3)+1
      case 1
        ids1  = add('1/ped_examples',      imdb.sets.TRAIN, 1) ;
        ids2  = add('1/non-ped_examples',  imdb.sets.TRAIN, 2) ;
        ids3  = add('2/ped_examples',      imdb.sets.TRAIN, 3) ;
        ids4  = add('2/non-ped_examples',  imdb.sets.TRAIN, 4) ;
      case 2
        ids1  = add('1/ped_examples',      imdb.sets.TRAIN, 1) ;
        ids2  = add('1/non-ped_examples',  imdb.sets.TRAIN, 2) ;
        ids3  = add('3/ped_examples',      imdb.sets.TRAIN, 5) ;
        ids4  = add('3/non-ped_examples',  imdb.sets.TRAIN, 6) ;
      case 3
        ids1  = add('2/ped_examples',      imdb.sets.TRAIN, 3) ;
        ids2  = add('2/non-ped_examples',  imdb.sets.TRAIN, 4) ;
        ids3  = add('3/ped_examples',      imdb.sets.TRAIN, 5) ;
        ids4  = add('3/non-ped_examples',  imdb.sets.TRAIN, 6) ;
    end
    switch floor((opts.split-1)/3)+1
      case 1
        ids5  = add('T1/ped_examples',     imdb.sets.TEST, 7) ;
        ids6  = add('T1/non-ped_examples', imdb.sets.TEST, 8) ;
      case 2
        ids5  = add('T2/ped_examples',     imdb.sets.TEST, 9) ;
        ids6  = add('T2/non-ped_examples', imdb.sets.TEST, 10) ;
    end
    imdb.classes.imageIds{1} = cat(2, ids1, ids3, ids5) ;
    imdb.classes.imageIds{2} = cat(2, ids2, ids4, ids6) ;
  end

  assert(numel(unique(imdb.images.id)) == numel(imdb.images.id )) ;
end
