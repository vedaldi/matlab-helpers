function imdb = imageDbFromVoc(root, varargin)
% IMAGEDBFROMVOC   Construct an image database from VOC data
%   IMDB = IMAGEDBFROMVOC(ROOT) builds an IMDB structure from
%   the VOC data at ROOT.
%
%   The VOCDevkit is assumed to have layout:
%
%   VOCDevkit/
%     VOCCode/
%     VOC2007/
%       Annotations/
%       ImageSets/
%       JPEGImages/
%       ...
%
%   The variant of the VOC edition to use is automatically detected based
%   on the version of the scripts in VOCDevkit/VOCCode. The 'edition'
%   option can be used to specity a particular edition:
%
%     imdb = imageDbFromVoc(root, 'edition', 'voc11') ;
%
%   Specfitying the edition is necessary for VOC2011 and VOC2012 as the
%   devkits are identical and cannot be autodetected.
%
%   The returned IMDB strucure has fields:
%
%   imdb.meta.sets = list of set names (train, val, test)
%   imdb.meta.classes = list of class names
%   imdb.meta.aspects = list of aspect names
%   imdb.imageDir = path to the dataset images
%
%   imdb.images.id = numerical ID of an image
%   imdb.images.name = image name (append to imdb.imageDir to get the path)
%   imdb.images.set = image set code
%
%   imdb.rois.id = numerical ID of a Region Of Interest (object instance)
%   imdb.rois.imageId = ID of the corresponding image
%   imdb.rois.set = object set code
%   imdb.rois.class = object class code
%   imdb.rois.box = object bounding box
%   imdb.rois.difficult = difficult object attribute
%   imdb.rois.truncated = truncated object attribute
%   imdb.rois.aspect = object aspect attribute
%
%   The structure-of-array format is used. For example imdb.images is a 1x1
%   structure and imdb.images.id is a row vector with one entry per image.
%   While this format is not logically optimal, it is much more efficient
%   in MATLAB.
%
%   Author:: Andrea Vedaldi

% AUTORIGHTS
% Copyright (C) 2008-09 Andrea Vedaldi
%
% This file is part of the VGG MKL Class and VGG MKL Det code packages,
% available in the terms of the GNU General Public License version 2.

opts.edition = [] ;
opts.sets = {'train','val','test'} ;
opts = vl_argparse(opts, varargin) ;

[VOCopts, edition] = getVocOpts(root, 'edition', opts.edition) ;
templ = VOCopts.imgsetpath ;

imdb.meta.sets = {'train', 'val', 'test'} ;
imdb.meta.classes = {...
  'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', ...
  'bus', 'car', 'cat', 'chair', 'cow', ...
  'diningtable', 'dog', 'horse', 'motorbike', 'person', ...
  'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor'} ;
imd.meta.aspects = {'front', 'rear', 'left', 'right', 'misc'}  ;
imdb.imageDir = root ;

% source images
set = 0 ;
sets = cell(1,3) ;
names = cell(1,3) ;
for setName = {'train','val','test'}  
  setPath = sprintf(VOCopts.imgsetpath, char(setName)) ;
  fprintf('%s: adding ''%s''\n', mfilename, setPath) ;
  
  setNames = textread(setPath, '%s') ;
  for i = 1:length(setNames)
    fullName = sprintf(VOCopts.imgpath, setNames{i}) ;
    fullName = fullName(length(root)+1:end) ;
    if fullName(1) == '/', fullName(1) = [] ; end ;
    setNames{i} = fullName ;
  end
  
  set = set + 1 ;
  sets{set} = repmat(set, 1, length(setNames)) ;
  names{set} = setNames' ;
end

names = horzcat(names{:}) ;
sets = horzcat(sets{:}) ;

imdb.images.id = 1:numel(names) ;
imdb.images.name = names ;
imdb.images.set = sets ;
imdb.images.size = zeros(2, numel(names)) ;

% verify all image
rois = {} ;
n = 0 ;

fprintf('%s: checking images and getting sizes ...\n', mfilename) ;
for i = 1:length(names)
  fprintf('\r%5.1f %%: %s', i/length(names)*100, names{i}) ;
  info = imfinfo(fullfile(imdb.imageDir, names{i})) ;
  imageSize = [info.Width; info.Height] ;
  imdb.images.size(:,i)  = imageSize ;
    
  %% ROIs
  [drop, name] = fileparts(names{i}) ;
  annoPath = sprintf(VOCopts.annopath, name) ;
  if ~exist(annoPath, 'file')
    warning('Could not find annotations for image ''%s'' (%s)', ...
            name, decodeEnum(imDb.sets, imageSet)) ;
    continue ;
  end

  anno = PASreadrecord(annoPath) ;
  for o = anno.objects
    n = n + 1 ;
    [~,c] = ismember(o.class, imdb.meta.classes) ;
    rois{n}.id = n ;
    rois{n}.imageId = imdb.images.id(i) ;
    rois{n}.set = imdb.images.set(i) ;
    rois{n}.class = c ;

    switch o.view
      case 'Frontal', rois{n}.aspect = 1 ;
      case 'Rear', rois{n}.aspect = 2 ;
      case {'SideFaceLeft', 'Left'}, rois{n}.aspect = 3 ;
      case {'SideFaceRight', 'Right'}, rois{n}.aspect = 4 ;
      case '', rois{n}.aspect = 5 ;
      otherwise, error(sprintf('Unknown view ''%s''', o.view)) ;
    end
    
    rois{n}.difficult = logical(o.difficult) ;
    rois{n}.truncated = logical(o.truncated) ;
    if isfield(o, 'occluded')
      rois{n}.occluded = logical(o.occluded) ;
    else
      rois{n}.occluded = false ;
    end
    rois{n}.box = [max(round(o.bbox(1:2)), 1)'; min(round(o.bbox(3:4)), imageSize')'] ;
  end
end
fprintf('\n') ;

rois = horzcat(rois{:}) ;

imdb.rois.id = [rois.id] ;
imdb.rois.imageId = [rois.imageId] ;
imdb.rois.set = [rois.set] ;
imdb.rois.class = [rois.class] ;
imdb.rois.aspect = [rois.aspect] ;
imdb.rois.difficult = [rois.difficult] ;
imdb.rois.truncated = [rois.truncated] ;
imdb.rois.occluded = [rois.occluded] ;
imdb.rois.box = [rois.box] ;

