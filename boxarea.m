function areas = boxarea(boxes)
% BOXAREA  Compute the are of a box
%    AREAS = BOXAREA(BOXES) returns the AREAS of the BOXES.
%    BOXES is a 4 x N matrix with one column for each box
%    and AREAS is a 1 x N vector of areas. A box is a vector
%    [XMIN YMIN XMAX YMAX].

% Author: Andrea Vedaldi

areas = prod([-1 0 1 0 ; 0 -1 0 1] * boxes + 1,1) ;
