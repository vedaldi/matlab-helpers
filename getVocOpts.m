function [VOCopts, edition] = getVocOpts(root, varargin)
% GETVOCOPTS  Retrieve the VOCopts structure from VOC 2007/2008 devkit
%   [VOCopts, EDITION] = GETVOCOPTS(ROOT) retrieves the VOCopts
%   structure from the VOC devkit found at ROOT and the edition
%   version string EDITION ('07', '08', ...).
%
%   Author:: Andrea Vedaldi

% AUTORIGHTS

opts.edition = [] ;
opts = vl_argparse(opts, varargin) ;

if isstr(opts.edition)
  switch opts.edition
    case 'voc07', opts.edition = '07' ;
    case 'voc08', opts.edition = '08' ;
    case 'voc09', opts.edition = '09' ;
    case 'voc10', opts.edition = '10' ;
    case 'voc11', opts.edition = '11' ;
    case 'voc12', opts.edition = '12' ;
  end
end

c = pwd ;
pathVOCInit = fullfile(root, 'VOCcode', 'VOCinit.m') ;
[s, edition] = patchVOCinit(pathVOCInit, opts.edition) ;
try
  cd(fullfile(root, 'VOCcode')) ;
  eval(s) ;
  cd(c) ;
catch ex
  cd(c) ;
  error('%s\nCould not source and patch VOCinit ''%s''', ex.message, pathVOCInit) ;
end

% --------------------------------------------------------------------
function [s, edition] = patchVOCinit(pathVOCinit, edition)
% --------------------------------------------------------------------

dirName = fileparts(pathVOCinit) ; % path/VOCcode/VOCinit.m -> path/VOCcode
dirName = fileparts(dirName) ; % path/VOCcode -> path

% source VOCinit.m
f = fopen(pathVOCinit,'r') ;
s = fread(f,+inf,'*char')' ;
fclose(f) ;

% compute a checksum
checksum = sum(double(s)) ;

% patch based on the version identified through the checksum
switch checksum
  case 262046
    % 2007 VOCcode
    if isempty(edition)
      edition = '07' ;
    end
    lineEnds = find(s == sprintf('\n')) ;
    header = sprintf('VOCopts.dataset=''%s'' ;\ncwd=''%s''; VOC2006=false;\n', ...
                     ['VOC20' edition], dirName) ;
    s = [header, s(lineEnds(18)+1:end)] ;

  case 270542
    % 2009 VOCcode
    if isempty(edition)
      edition = '08' ;
    end
    edition = '08' ;
    lineEnds = find(s == sprintf('\n')) ;
    header = sprintf('VOCopts.dataset=''%s'' ;\ndevkitroot=''%s'';\n', ...
                     ['VOC20' edition], dirName) ;
    s = [header, s(lineEnds(16)+1:end)] ;

  case 322164
    % 2010 VOCcode
    if isempty(edition)
      edition = '10' ;
    end
    lineEnds = find(s == sprintf('\n')) ;
    header = sprintf('VOCopts.dataset=''%s'' ;\ndevkitroot=''%s'';\n', ...
                    ['VOC20' edition], dirName) ;
    s = [header, s(lineEnds(13)+1:end)] ;

  case 329427
    % 2011 VOCcode or 2012 (the devkit did not change)
    if isempty(edition)
      edition = '11' ;
      warning('VOC2011 and 2012 devkits are the same. Configuring for 2011 data by default.') ;
    end
    lineEnds = find(s == sprintf('\n')) ;
    header = sprintf('VOCopts.dataset=''%s'' ;\ndevkitroot=''%s'';\n', ...
                    ['VOC20' edition], dirName) ;
    s = [header, s(lineEnds(13)+1:end)] ;

  otherwise
    error('The file ''%s'' does not match any of the known VOCinit.m versions.', ...
          name) ;
end
