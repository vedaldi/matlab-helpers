function B = pad2(A, left, top, right, bottom, varargin)
% PAD2 Pad array in the first 2 dimension
%  B = PAD2(A, LEFT, TOP, RIGHT, BOTTOM) pads the array A by LEFT,
%  TOP, RIGHT, BOTTOM null elements along the corresponding
%  dimensions. LEFT, TOP, RIGHT and BOTTOM can be negative, in
%  which case corresponding rows or columns are removed. For
%  multi-dimensional arrays, padding happens in the first two
%  dimensions.
%
%  Value:: [0]
%    Specify the padding value, i.e. the value used to fill any
%    new elment added to the array.
%
% Author: Andrea Vedaldi

opts.value = 0 ;
opts = vl_argparse(opts,varargin) ;

w = size(A,2) ;
h = size(A,1) ;

w_ = w + left + right ;
h_ = h + top + bottom ;

dims = size(A) ;
other = num2cell(dims(3:end)) ;

B = zeros(max(h_,0), ...
          max(w_,0), ...
          other{:}, ...
          class(A)) ;
if opts.value, B = B + opts.value ; end

u0 = max(1+left,1) ;
u1 = min(w_-right,w_) ;
u0_ = max(1-left,1) ;
u1_ = min(w+right,w) ;

v0 = max(1+top,1) ;
v1 = min(h_-bottom,h_) ;
v0_ = max(1-top,1) ;
v1_ = min(h+bottom,h) ;

B(v0:v1,u0:u1,:) = A(v0_:v1_,u0_:u1_,:) ;
