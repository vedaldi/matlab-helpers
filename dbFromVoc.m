function [imdb, roidb] = dbFromVoc(vocDir, varargin)
% DBFROMVOC  Setup databases for VOC data
%   Assume that 'data/VOCdevkit_2010' points to the 2010 edition of
%   the VOC data. Then:
%
%     DBFROMVOC('data/VOCdevkit_2010', 'dataDir', 'data/voc10') ;
%
%   saves an image database to 'data/voc10/imdb.mat' and a ROI
%   database to 'data/voc10/roidb.mat' for the VOC 2010 data.
%
%   Options:
%
%   dataDir:: []
%     The database will be saved here if this string is not empty.
%
%   imdbPath:: []
%     Where to save the image database, if not empty. If it is
%     empty, but dataDir is not, then a default value is used.
%
%   roidbPath:: []
%     Similar to imdbPath, but for the ROI database.
%
%   clobber:: true
%     Whether to override existing files.
%
%   sets:: []
%     A list of subset of data to use. If empty, the list is set to
%     {'train<X>', 'val<X>', 'test<X>'}, where <X> is the value
%     of the edition option.
%
%   edition:: []
%     Specify a version of the data to load. If empty, this is
%     determined from the VOC devkit. However, for 2011 and 2012
%     this is not possible as the devkit is unchanged.

opts.sets = [] ;
opts.dataDir = [] ;
opts.imdbPath = [] ;
opts.roidbPath = [] ;
opts.clobber = true ;
opts.edition = [] ;
opts = vl_argparse(opts, varargin) ;

if ~exist(vocDir, 'dir')
  error('VOC directory ''%s'' is not a directory', vocDir) ;
end

[VOCconf, edition] = getVocOpts(vocDir, 'edition', opts.edition) ;
fprintf('%s: found 20%s edition\n', mfilename, edition) ;

if isempty(opts.sets)
  opts.sets = {['train' edition], ...
               ['val', edition], ...
               ['test', edition]} ;
end

if ~isempty(opts.dataDir)
  if isempty(opts.imdbPath)
    opts.imdbPath = fullfile(opts.dataDir, 'imdb.mat') ;
  end
  if isempty(opts.roidbPath)
    opts.roidbPath = fullfile(opts.dataDir, 'roidb.mat') ;
  end
end

fprintf('%s: parameters:\n', mfilename) ;
disp(opts) ;

if ~isempty(opts.imdbPath) & exist(opts.imdbPath, 'file') & ~opts.clobber
  fprintf('%s: loading %s\n', mfilename, opts.imdbPath) ;
  imdb = load(opts.imdbPath) ;
else
  imdb = imageDbFromVoc(vocDir, 'sets', opts.sets, 'edition', opts.edition) ;
  if ~isempty(opts.imdbPath)
    fprintf('%s: saving %s\n', mfilename, opts.imdbPath) ;
    vl_xmkdir(fileparts(opts.imdbPath)) ;
    save(opts.imdbPath, '-STRUCT', 'imdb') ;
  end
end

if ~isempty(opts.roidbPath) & exist(opts.roidbPath, 'file') & ~opts.clobber
  roidb = load(opts.roidbPath) ;
else
  roidb = roiDbFromVoc(vocDir, imdb, 1:length(imdb.images.id), 'edition', opts.edition) ;
  if ~isempty(opts.roidbPath)
    fprintf('%s: saving %s\n', mfilename, opts.roidbPath) ;
    vl_xmkdir(fileparts(opts.roidbPath)) ;
    save(opts.roidbPath, '-STRUCT', 'roidb') ;
  end
end
