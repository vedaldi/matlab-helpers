function [posterior, loglik] = gmmtest(gmm, x)
% GMMTEST Test Gaussian Mixture Model
%   [POSTERIOR, LOGLIK] = GMMTEST(GMM, X) returns for each column X
%   the posterior distribution
%
%     POSTERIOR(:,i) = [P(k = 1 | X(:,i)); ... P(k = K | X(:,i))]
%
%   where P(k | x) is the posterior distribution over the K components
%   of the GMM model for the data point x. The function also
%   computes the log likelihood
%
%     LOGLIK(i) = log(P(X(:,i)).
%
%   Author:: Andrea Vedaldi

  numData = size(x,2) ;
  dimension = size(x,1) ;
  numGmmComponents = size(gmm.mean, 2) ;

  % compute log P(x | k)
  logp = zeros(numData, numGmmComponents) ;
  for k = 1:numGmmComponents
    % remove the mean, divide by sigma^2
    dx = bsxfun(@minus, x, gmm.mean(:,k)) ;
    dx_sigma2 = bsxfun(@times, dx, 1./gmm.variance(:,k)) ;
    logp(:,k) = ...
        - 0.5 * sum(dx .* dx_sigma2 , 1) ...
        - 0.5 * sum(log(gmm.variance(:,k))) ...
        - 0.5 * dimension * log(2*pi) ;
  end

  % Compute the posterior P(k | x) and the log likelihood log P(x)
  % for all points. To avoid numerical difficulties, divides
  % P(x | k) by max_k P(x | k) beforehand.

  maxlogp = max(logp, [], 2) ;
  tmp = exp(bsxfun(@minus, logp, maxlogp)) ;
  loglik = maxlogp + log(tmp * gmm.prior')  ;
  posterior = bsxfun(@times, tmp, gmm.prior) ;
  posterior = bsxfun(@times, posterior, 1./sum(posterior,2)) ;
end
