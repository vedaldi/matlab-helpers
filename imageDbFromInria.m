function imdb = imageDbFromInria(root, varargin)
% IMAGEDBFROMINRIA   Construct an image database from INRIA data
%   Author:: Andrea Vedaldi

% AUTORIGHTS
% Copyright (C) 2008-09 Andrea Vedaldi
%
% This file is part of VGG MKL classification and detection code,
% available in the terms of the GNU General Public License version 2.

opts.autoDownload = true ;
opts.VOCdevkitPath = [] ;
opts = vl_argparse(opts, varargin) ;

if isempty(opts.VOCdevkitPath)
  opts.VOCdevkitPath = fullfile(root, 'VOCdevkit') ;
end

%% Download data if neeeded
if ~exist(fullfile(root, 'Train', 'pos.lst'), 'file') 
  if opts.autoDownload
    fprintf('%s: INRIAPerson data not found; autodownloading ...\n', mfilename) ;
    [parentDir, datasetDir] = fileparts(root) ;
    untar('http://pascal.inrialpes.fr/data/human/INRIAPerson.tar', mfilename, parenDir) ;
    if ~isequal(datasetDir, 'INRIAPerson')
      movefile(...
        fullfile(parentDir, 'INRIAPerson'), ...
        fullfile(parentDir, datasetDir)) ;
    end
  else
    error('Could not find INRIAPerson in ''%s'' and autodownload is disabled', root) ;
  end
end

if ~exist('PASreadrecord')
  fprintf('%s: VOCdevkit not in MATLAB path; trying to use a local copy\n', mfilename) ;
  if ~exist(opts.VOCdevkitPath)
    if opts.autoDownload
      [parentDir, devkitDir] = fileparts(opts.VOCdevkitPath) ;
      fprintf('%s: autodownloading a copy of the VOCdevkit to %s\n', mfilename, opts.VOCdevkitPath) ;
      untar('http://pascallin.ecs.soton.ac.uk/challenges/VOC/voc2012/VOCdevkit_18-May-2011.tar', parentDir) ;
      if ~isequal(devkitDir, 'VOCdevkit')
        movefile(...
          fullfile(parentDir, 'VOCdevkit'), ...
          fullfile(parentDir, devkitDir)) ;
      end
    else
      error('Could not find a copy of the VOCDevkit in %s/VOCDevkit and autodownload is disabled', root) ;
    end
  end
  VOCcodePath = fullfile(opts.VOCdevkitPath, 'VOCcode') ;
  fprintf('%s: adding %s to MATLAB path\n', VOCcodePath) ;
  addpath(VOCcodePath) ;
end

%% Source image lists
imdb.meta.sets = {'train','val','test'} ;
imdb.meta.aspects = {'misc'} ;
imdb.meta.classes = {'person', 'background'} ;
imdb.imageDir = root ;

names = cell(1,4) ;
sets = cell(1,4) ;
classes = cell(1,4) ;
listNames = {'Train/pos.lst', 'Train/neg.lst', 'Test/pos.lst', 'Test/neg.lst'} ;
listSets = [1 1 3 3] ;
listClasses = [1 2 1 2] ;
for i = 1:numel(listNames)
    list = textread(fullfile(root, listNames{i}), '%s') ;
    names{i} = list' ;
    sets{i} = listSets(i) * ones(1, numel(list)) ;
    classes{i} = listClasses(i) * ones(1, numel(list)) ;
end
names = horzcat(names{:}) ;
sets = horzcat(sets{:}) ;
classes = horzcat(classes{:}) ;

imdb.images.id = 1:numel(names) ;
imdb.images.name = names ;
imdb.images.set = sets ;
imdb.images.class = classes ;
imdb.images.size = zeros(2, numel(names)) ;

%% Scan images and source ROIs
rois = {} ;
n = 0 ;

fprintf('%s: checking images and getting sizes ...\n', mfilename) ;
for i = 1:length(imdb.images.name)
  fprintf('\r%s: sourcing image %s (%5.1f %%)', mfilename, imdb.images.name{i}, i/length(names)*100) ;
  info = imfinfo(fullfile(imdb.imageDir, imdb.images.name{i})) ;
  imageSize = [info.Width; info.Height] ;
  imdb.images.size(:,i)  = imageSize ;
    
  if imdb.images.class(i) == 2
    % negative images has no ROIs
    continue ;
  end
  [~, name] = fileparts(imdb.images.name{i}) ;    
  if imdb.images.set(i) == 1
    annoPath = fullfile(root, 'Train', 'annotations', [name, '.txt']) ;
  else
    annoPath = fullfile(root, 'Test', 'annotations', [name, '.txt']) ;
  end

  if ~exist(annoPath, 'file')
    warning('Could not find annotations for image ''%s'' (%s)', ...
            imdb.images.name{i}, decodeEnum(imDb.sets, imageSet)) ;
    continue ;
  end

  anno = PASreadrecord(annoPath) ;
  for o = anno.objects
    n = n + 1 ;
    [~,c] = ismember(o.class, imdb.meta.classes) ;
    rois{n}.id = n ;
    rois{n}.imageId = imdb.images.id(i) ;
    rois{n}.set = imdb.images.set(i) ;
    rois{n}.class = c ;

    switch o.view
      case 'Frontal', rois{n}.aspect = 1 ;
      case 'Rear', rois{n}.aspect = 2 ;
      case {'SideFaceLeft', 'Left'}, rois{n}.aspect = 3 ;
      case {'SideFaceRight', 'Right'}, rois{n}.aspect = 4 ;
      case '', rois{n}.aspect = 5 ;
      otherwise, error(sprintf('Unknown view ''%s''', o.view)) ;
    end
    
    rois{n}.difficult = logical(o.difficult) ;
    rois{n}.truncated = logical(o.truncated) ;
    if isfield(o, 'occluded')
      rois{n}.occluded = logical(o.occluded) ;
    else
      rois{n}.occluded = false ;
    end
    rois{n}.box = [max(round(o.bbox(1:2)), 1)'; min(round(o.bbox(3:4)), imageSize')'] ;
  end
end
fprintf('\n') ;

rois = horzcat(rois{:}) ;

imdb.rois.id = [rois.id] ;
imdb.rois.imageId = [rois.imageId] ;
imdb.rois.set = [rois.set] ;
imdb.rois.class = [rois.class] ;
imdb.rois.aspect = [rois.aspect] ;
imdb.rois.difficult = [rois.difficult] ;
imdb.rois.truncated = [rois.truncated] ;
imdb.rois.occluded = [rois.occluded] ;
imdb.rois.box = [rois.box] ;

