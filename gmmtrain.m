function [gmm, loglik] = gmmtrain(x, k, varargin)
% GMMTRAIN Train Gaussian Mixture Model
%   GMM = GMMTRAIN(X,K) fits a GMM with K components to the data X.
%   The D x N matrix X contains N data samples of dimension D.  GMM is
%   a structure with fields
%
%   GMM.MEAN: D x K matrix of the means of the Gaussians.
%   GMM.VARIANCE: D x K matrix of the diagonal covariances of the Gaussians.
%   GMM.PRIOR: 1 x K vector of the prior probabilities of the components.
%
%   [GMM, LOGLIK] = GMMTRAIN(...) returns the log-likelihood of the
%   solution too.
%
%   Options:
%
%   Verbose:: false
%     Be verbose.
%
%   MaxNumIterations:: 100
%     Maximum number of EM iterations.
%
%   MinVarianceRel:: 0.01
%     The variance of each dimension of each Gaussian component is at
%     least MinVarianceRel times the sample variance of the corresponding
%     data component.
%
%   MinVarianceAbs:: 1e-9
%     The variance of each dimension of each Gaussiain component is
%     at least MinVarianceAbs.
%
%   MinPrior:: 0.1 / K
%     Minimum prior of each GMM component.
%
%   MinPriorRestart:: 1 / SIZE(X,2)
%     Minimum posterior of each component before re-initialization
%     is attempted.
%
%   Seed:: 0
%     Random seed (for initialization).
%
%   Epsilon:: 1e-3
%     Convergence threhsold. The algorithm stops when the increase in
%     log-likelihood for the past four iterations has been less than
%     EPSILON times the increase in the log-likelihood from the first
%     iteration.
%
%   Author:: Andrea Vedaldi

  opts.verbose = false ;
  opts.maxNumIterations = 100 ;

  opts.minPrior = 0.1 / k ;
  opts.minPriorRestart = 1 / size(x,2) ;
  opts.minVarianceRel = 0.01 ;
  opts.minVarianceAbs = 1e-9 ;

  opts.seed = 0 ;
  opts.numRepetitions = 1 ;
  opts.epsilon = 1e-3 ;
  opts = vl_argparse(opts, varargin) ;

  if opts.verbose
    fprintf('%s: clustering %d vectors into %d Gaussian clusters\n', ...
            mfilename, size(x,2), k) ;
    fprintf('%s: maxNumIterations = %d\n', mfilename, opts.maxNumIterations) ;
    fprintf('%s: numRepetitions = %d\n', mfilename, opts.numRepetitions) ;
    fprintf('%s: minVarianceAbs = %d\n', mfilename, opts.minVarianceAbs) ;
    fprintf('%s: minVarianceRel = %d\n', mfilename, opts.minVarianceRel) ;
    fprintf('%s: minPrior = %d\n', mfilename, opts.minPrior) ;
    fprintf('%s: seed = %f\n', mfilename, opts.seed) ;
    fprintf('%s: epsilon = %f\n', mfilename, opts.epsilon) ;
  end

  % repeat training a number of times and keep the best solution
  elapsed = tic ;
  for t = 1:opts.numRepetitions
    fprintf('%s: repetition %d\n', mfilename, t) ;
    [gmm{t}, loglik(t)] = train(opts, x, k) ;
    opts.seed = opts.seed + 1 ;
  end

  [loglik, best] = max(loglik) ;
  gmm = gmm{best} ;
  fprintf('%s: final log-likelihood: %.5g\n', mfilename, loglik) ;
  fprintf('%s: ended in %.2g minutes\n', mfilename, toc(elapsed) / 60) ;
end

% --------------------------------------------------------------------
function [gmm, loglik] = train(opts, x, k)
% --------------------------------------------------------------------

  % initialize the assignemnts with K-means
  n = size(x,2) ;
  d = size(x,1) ;
  vl_twister('state',opts.seed) ;
  [centers, kassign] = vl_kmeans(x, k, 'algorithm', 'elkan') ;

  % divide the data into chunks
  numData = size(x,2) ;
  numChunks = max(matlabpool('size'), 1) ;
  data = Composite() ;
  lik = Composite() ;
  posterior = Composite() ;

  for i = 1:numChunks
    chunk = i:numChunks:numData ;
    data{i} = x(:, chunk) ;
    lik{i} = - inf(1, numel(chunk)) ;
    tmp = zeros(numel(chunk), k) ;
    tmp((1:numel(chunk)) + numel(chunk) * (double(kassign(chunk))-1)) = 1 ;
    posterior{i} = tmp ;
  end
  x = cat(2, data{:}) ;
  clear tmp ;

  % compute the minimum variances for each Gaussian component
  minSigma2 = max(opts.minVarianceRel * var(x, 0, 2), opts.minVarianceAbs) ;

  % do EM iterations
  for t = 1:opts.maxNumIterations

    % maximization: update the prior, mu, sigma2
    posterior_ = cat(1, posterior{:}) ;

    gmm.prior = mean(posterior_,1) ;
    gmm.prior = max(gmm.prior, opts.minPrior) ;
    gmm.prior = gmm.prior / sum(gmm.prior) ;

    weights = bsxfun(@times, posterior_, 1./sum(posterior_,1)) ;
    gmm.mean = x * weights ;
    gmm.variance = (x.*x) * weights - gmm.mean.*gmm.mean ;
    gmm.variance = bsxfun(@max, gmm.variance, minSigma2) ;

    % reinitialize any component with very small prior
    restart = find(gmm.prior < opts.minPriorRestart) ;
    if ~isempty(restart)
      fprintf('%s: restarting %d components\n', ...
              mfilename, numel(restart)) ;
      gmm.mean(:,restart) = vl_colsubset(x, numel(restart)) ;
    end

    % expectation: estimate the posteriors, log-likelihood
    spmd
      [posterior, loglik] = gmmtest(gmm, data) ;
    end
    loglik_ = cat(1, loglik{:}) ;
    E(t) = sum(loglik_) ;

    fprintf('%s:%d: log-likelihood: %f, prior in [%f %f]\n', ...
            mfilename, t, E(t), min(gmm.prior), max(gmm.prior)) ;

    % stopping condition
    if t > 5 && (E(t) - E(t-4)) < max((E(t) - E(1)) * opts.epsilon, 1e-12)
      fprintf('%s: convergence criterion met after %d iterations\n', ...
              mfilename, t) ;
      break ;
    end
  end

  if opts.verbose
    figure(1) ; clf ;
    subplot(1,2,1) ; plot(E) ; grid on ;
    title('log-likelihood') ;
    xlabel('iteration') ;
    subplot(1,2,2) ;
    bar(gmm.prior) ; grid on ;
    title('component prior') ;
    drawnow ;
  end

  if opts.verbose & d == 2
    figure(2) ; clf ;
    vl_plotframe(x,'r') ; hold on ;
    vl_plotframe(gmm.mean, 'g') ;
    mu_ = [gmm.mean; sqrt(gmm.variance(1,:)) ; ...
           zeros(1,k) ; sqrt(gmm.variance(2,:))] ;
    vl_plotframe(mu_, 'g') ;
    drawnow ;
  end

  loglik = E(end) ;
  fprintf('%s: log-likelihood = %.5g\n', mfilename, loglik) ;
end
