function imdb = imdbFromScene15(scene15Path, varargin)
% IMDBFROMSCENE15  Compute an image database from the VOC dataset
%   IMDB = IMDBFROMSCENE15(VOCPATH, VOCEDITION)
%
%   Example::
%     To generate a database IMDB for Scene 15
%
%       imdb = imdbFromVoc('/path/to/scene15', 'split', 1)

  opts.split = 1 ;
  opts = vl_argparse(opts, varargin) ;

  imdb.images.id = [] ;
  imdb.images.set = uint8([]) ;
  imdb.images.name = {} ;
  imdb.images.class = [] ;
  imdb.images.size = zeros(2,0) ;
  imdb.dir = scenePath ;
  imdb.classes.name = {'bedroom', 'CALsuburb', 'industrial', 'kitchen', 'livingroom', ...
                      'MITcoast', 'MITforest', 'MIThighway', 'MITinsidecity', 'MITmountain', ...
                      'MITopencountry', 'MITstreet', 'MITtallbuilding', 'PARoffice', 'store'} ;
  imdb.classes.imageIds = {} ;
  imdb.sets.TRAIN = uint8(1) ;
  imdb.sets.TEST = uint8(2) ;

  function ids = add(ims, setIds)
    ids = [] ;
    for i=1:length(ims)
      j = j + 1 ;
      ids(i) = j ;
      imdb.images.id(j) = ids(i) ;
      imdb.images.set(j) = setIds(i) ;
      imdb.images.name{j} = fullfile(imdb.classes.name{ci}, ims(i).name) ;
      imdb.images.class(j) = uint8(ci) ;
      info = imfinfo(fullfile(imdb.dir, imdb.images.name{j})) ;
      imdb.images.size(:,j) = [info.Width ; info.Height] ;
      fprintf('\radded %s', imdb.images.name{j}) ;
    end
  end

  j = 0 ;
  for ci = 1:length(imdb.classes.name)
    ims = dir(fullfile(imdb.dir, imdb.classes.name{ci}, '*.jpg')) ;

    setIds = repmat(imdb.sets.TEST, 1, numel(ims)) ;
    rand('state', split) ;
    setIds(vl_colsubset(1:numel(ims), 100)) = imdb.sets.TRAIN ;

    ids = add(ims(setIds==imdb.sets.TRAIN), setIds(setIds==imdb.sets.TRAIN)) ;
    ids = [ids add(ims(setIds==imdb.sets.TEST), setIds(setIds==imdb.sets.TEST))] ;
    imdb.classes.imageIds{ci} = ids ;
  end
end
