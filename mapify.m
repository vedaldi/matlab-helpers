function [map,xr,yr] = mapify(x,y,values)
% MAPIFY  Arranges features in a map
%   [MAP, XR, YR] = MAPIFY(X,Y,VALUES)

xr = unique(x(:)) ;
yr = unique(y(:)) ;

j = vl_binsearch(xr, x) ;
i = vl_binsearch(yr, y) ;

loc = [i(:) j(:)] ;
values = values(:) ;

t = 1 ;
while ~isempty(values)
  map{t} = zeros(numel(yr), numel(xr), class(values)) ;
  [loc_, sel] = unique(loc, 'rows', 'first') ;
  indexes =  loc_(:,1) + (loc_(:,2) - 1) * numel(yr) ;
  map{t}(indexes) = values(sel) ;
  loc(sel,:) = [] ;
  values(sel) = [] ;
  t = t + 1 ;
end

map = cat(3, map{:}) ;

