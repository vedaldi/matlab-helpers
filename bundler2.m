function [state, info] = bundler2(state, a, iHa, b, varargin)
% BUNLDER2  Bundle method with quadratic regularizer
%   BUNDLER2() sovles the problem
%
%     min_{w,xi} 1/2 w'H w + xi,  such that
%     xi >= b_t - <a_t, w>, for t = 1, ..., T.
%
%   Note that xi is a scalar variable, as this is a one-slack
%   formulation of the problem, and H is a possibly non-diagonal
%   regularization matrix. Optionally, BUNDLER2() also enforces
%   additional hard constraints:
%
%     <a_p,w> >= b_p,  for p = 1, ..., P.
%
%   BUNDLER2() solves the problem in the dual. Introducing Lagrange
%   multipliers alpha_i one obtains the problem:
%
%     max_{alpha>=0} min_{w,xi} 1/2 w'H w
%                               + xi
%                               + sum_t alpha_t (b_t - <a_t,w> - xi)
%                               + sum_p alpha_p (b_p - <a_p,w>).
%
%   Minimizing w.r.t. to xi yields the condition sum_t alpha_t = 1 (if
%   the domain is restricted to xi >=0 at the beginning, then this
%   constraints is relaxed to sum_t alpha_t <= 1). Then the optimal
%   value of w is given by
%
%     w = inv(H)  (sum_i alpha_i a_i)
%
%   where i spans both soft and hard constraints. In matrix form, this
%   relation writes
%
%     w = inv(H) A alpha
%
%   where A = [a_1 ... a_{T+P}]. The dual problem is
%
%     max_{alpha>=0}  sum_i alpha_i b_i - 1/2 alpha' K alpha
%     such that sum_t alpha_t <= 1.
%
%   where K = A'inv(H)A is the kernel matrix. Note that the upper
%   bound on the sum of alphas involves only the soft constraints, and
%   this is the only way they are distinguished in the dual.
%
%   USAGE
%
%   Bunlder adds one constraint per time to the bundle. Initially,
%   the problem is initialized with an empty set of constraints by:
%
%    STATE = BUNDLER2()
%
%   Then constraints are added one per time by
%
%    STATE = BUNDLER2(STATE, A, IHA, B)
%
%   where A is a vector, IHA = inv(H)*A, and B is a scalar (this is
%   used in constraint generation typically). By default, each added
%   constraint is soft. To add an hard constraint, use:
%
%    STATE = BUNDLER2(STATE, A, IHA, B, 'SOFT', FALSE)
%
%   After eachiteration, the state can be read-off:
%
%    STATE.DUALOBJECTIVE: the optimum of the dual problem
%    STATE.DUALVARIABLES: the vector alpha
%    STATE.SOFTVARIABLES: a boolean vector indicating soft/hard vars
%    STATE.IHA: the matrix inv(H) A = inv(H) [a_1 ... a_{T+P}]
%    STATE.B: the vector B = [b_1 ... b_{T+P}]
%    STATE.K: the matrix A'inv(H)A
%
%   not to mention:
%
%    INFO.W: the vector w = inv(H) A alpha
%    INFO.REGULARIZER: the value 1/2 w' H w
%    INFO.SLACK: the value of xi

opts.soft = true ;
opts = vl_argparse(opts, varargin) ;
opts.soft = double(opts.soft) ;

if nargin == 0
  state.iHa = [] ;
  state.b = [] ;
  state.softVariables = [] ;
  state.dualVariables = [] ;
  state.dualAge = [] ;
  state.dualObjective = -inf ;
  state.K = [] ;
  state.diagonalFix = 0 ; %1e-8 ;
  state.maxNumIterations = 1000 ;
  state.verbose = false ;
  state.solver = 'matlab' ;

  info.w = [] ;
  info.slack = [] ;
  info.regularizer = [] ;

  % this will go
  state.w = info.w ;
  state.slack = info.slack ;
  state.regularizer = info.regularizer ;
  return ;
end

if nargin > 1
  dimension = size(a,1) ;
  numNewConstraints = size(a,2) ;
  if isempty(state.iHa)
    %state.a = zeros(dimension, 0) ;
    state.iHa = zeros(dimension, 0) ;
  end

  % add new constraints to the pool
  state.dualVariables = [state.dualVariables ; zeros(numNewConstraints,1)] ;
  state.softVariables = [state.softVariables ; opts.soft] ;
  state.dualAge       = [state.dualAge ; 0] ;

  % add missing part of kernel matrix
  K11 = state.K ;
  K21 = a' * state.iHa ;
  K22 = a' * iHa + state.diagonalFix ;
  state.K = [K11 K21' ; K21 K22] ;
  state.b = [state.b, b] ;
  state.iHa = [state.iHa, iHa] ;
end

switch state.solver
  case 'matlab', [state,active,info] = matlabSolver(state) ;
  case 'mosek', [state,active,info] = mosekSolver(state) ;
end

% remove idle variables
state.dualAge = state.dualAge + 1 ;
active = active | ~state.softVariables ;
state.dualAge(active) = 0 ;
keep = state.dualAge < 25 ;

state.dualVariables = state.dualVariables(keep) ;
state.softVariables = state.softVariables(keep) ;
state.dualAge = state.dualAge(keep) ;
state.K = state.K(keep,keep) ;
%state.a = state.a(:,keep) ;
state.b = state.b(keep) ;
state.iHa = state.iHa(:,keep) ;

% update the model
info.w = state.iHa * state.dualVariables ;
info.regularizer = state.b * state.dualVariables - state.dualObjective ;
info.slack = state.dualObjective - info.regularizer;

% this will go
state.w = info.w ;
state.slack = info.slack ;
state.regularizer = info.regularizer ;

% --------------------------------------------------------------------
function [state,active,info] = matlabSolver(state)
% --------------------------------------------------------------------

% quadprog opts
% LargeScale=off -> active set algo (only one supported here)
quadopts = {'LargeScale', 'off', ...
            'Algorithm', 'active-set', ...
            'MaxIter', state.maxNumIterations} ;
if ~state.verbose
  quadopts = {quadopts{:}, 'Diagnostic', 'off', 'Display', 'off'} ;
else
  quadopts = {quadopts{:}, 'Diagnostic', 'on', 'Display', 'testing'} ;
end

% solve quadprog
for retry = 1:3
  [state.dualVariables, state.dualObjective, flag, output, lagrange] = ...
      quadprog(state.K, -state.b, ...
               state.softVariables', 1, ...
               [],[], ...
               zeros(length(state.dualVariables), 1), [], ...
               state.dualVariables, ...
               optimset(quadopts{:})) ;
  state.dualObjective = - state.dualObjective ;
  info.numIterations = output.iterations ;
  info.converged = (flag ~= 0) ;
  if info.converged, break ; else
    warning('quadprog reached the maximum number of iterations (%d); retry %d/3', ...
            state.maxNumIterations, retry) ;
  end
end
if ~info.converged
  warning('quadprog did not converge. Giving up.') ;
end

active = (lagrange.lower < 1e-3) ;

% --------------------------------------------------------------------
function [state,active,info] = mosekSolver(state)
% --------------------------------------------------------------------

% [res] = mskqpopt(q,c,a,blc,buc,blx,bux,param,cmd)

param.MSK_DPAR_INTPNT_TOL_REL_GAP = 1e-10 ;

res = mskqpopt(state.K, -state.b, ...
               state.softVariables', [], 1, ...
               zeros(length(state.dualVariables), 1), [], ...
               param, 'minimize info echo(0)') ;
state.dualVariables = res.sol.itr.xx ;
state.dualObjective = -res.sol.itr.pobjval ;


info.numIterations = 1 ;
info.converged = strcmp(res.sol.itr.solsta, 'OPTIMAL') ;
if ~info.converged
  warning('mosek did not converge.') ;
end

active = (res.sol.itr.slx < 1e-3) ;
