#include "mex.h"
#include <math.h>
#include <strings.h>
#include <string.h>

#define max(x,y) (((x)<=(y))?(y):(x))
#define min(x,y) (((x)>=(y))?(y):(x))

#define IN_IMAGE 0

/* ---------------------------------------------------------------- */
void mexFunction (int nout, mxArray * out [],
                  int nin, const mxArray * in [])
{
  mxArray const * image_array = in[IN_IMAGE] ;

  if (nin != 1) {
    mexErrMsgTxt("One argument expected.") ;
  }
  if (nout > 1) {
    mexErrMsgTxt("More than one output arguments requested") ;
  }
  if (!mxIsNumeric(image_array) ||
      mxGetNumberOfDimensions(image_array) > 2  ||
      mxGetClassID(image_array) != mxDOUBLE_CLASS ||
      mxIsComplex(image_array)) {
    mexErrMsgTxt("IMAGE is not a plain matrix.") ;
  }

  {
    int width = mxGetN(in[IN_IMAGE]) ;
    int height = mxGetM(in[IN_IMAGE]) ;
    double * labels = mxGetPr(in[IN_IMAGE]) ;

    out[0] = mxCreateDoubleMatrix(height,width,mxREAL) ;
    double * components = mxGetPr(out[0]) ;
    int * open = mxMalloc(sizeof(int) * width*height) ;
    int numOpen = 0 ;
    int numComponents = 0 ;

    int x,y ;

    for(x = 0 ; x < width ; ++x) {
      for(y = 0 ; y < height ; ++y) {
        int index = x * height + y ;
        if (components[index]) continue ;
        components[index] = ++ numComponents ;
        open[numOpen++] = index ;
        double label = labels[index] ;
        while (numOpen) {
          int closing = open[--numOpen] ;
          int expand ;
          int xc = closing / height ;
          int yc = closing % height ;
          if (xc > 0) {
            expand = (xc - 1) * height + yc ;
            if (components[expand] == 0 && labels[expand] == label) {
              open[numOpen++]  = expand ;
              components[expand] = numComponents ;
            }
          }
          if (yc > 0) {
            expand = (xc) * height + (yc - 1) ;
            if (components[expand] == 0 && labels[expand] == label) {
              open[numOpen++]  = expand ;
              components[expand] = numComponents ;
            }
          }
          if (xc < width-1) {
            expand = (xc + 1) * height + yc ;
            if (components[expand] == 0 && labels[expand] == label) {
              open[numOpen++]  = expand ;
              components[expand] = numComponents ;
            }
          }
          if (yc < height-1) {
            expand = (xc) * height + (yc + 1) ;
            if (components[expand] == 0 && labels[expand] == label) {
              open[numOpen++]  = expand ;
              components[expand] = numComponents ;
            }
          }
        }
      }
    }
    mxFree(open) ;
  }
}
