MEX ?= mex
MEXEXT ?= $(shell mexext)

MACOSX_DEPLOYMENT_TARGET ?= 10.4

ifeq ($(MEXEXT),mexmaci)
ARCH ?= maci
SDKROOT ?= $(shell xcodebuild -version -sdk macosx | sed -n '/^Path\:/p' | sed 's/^Path: //')
STD_CFLAGS += -m64 -isysroot $(SDKROOT) -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
STD_LDFLAGS += -Wl,-syslibroot,$(SDKROOT) -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
CC=$(shell xcrun -sdk macosx -find gcc)
CXX=$(shell xcrun -sdk macosx -find g++)
endif

ifeq ($(MEXEXT),mexmaci64)
ARCH ?= maci64
SDKROOT ?= $(shell xcodebuild -version -sdk macosx | sed -n '/^Path\:/p' | sed 's/^Path: //')
STD_CFLAGS += -m64 -isysroot $(SDKROOT) -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
STD_LDFLAGS += -Wl,-syslibroot,$(SDKROOT) -mmacosx-version-min=$(MACOSX_DEPLOYMENT_TARGET)
CC=$(shell xcrun -sdk macosx -find gcc)
CXX=$(shell xcrun -sdk macosx -find g++)
endif

ifeq ($(MEXEXT),mexglx)
ARCH ?= glnx32
endif

ifeq ($(MEXEXT),mexa64)
ARCH ?= glnxa64
MEXFLAGS := -largeArrayDims
endif

MATLABROOT := $(shell $(MEX) -v 2>&1 | grep "MATLAB  * =" | sed 's/->.*= //g')
MEXFLAGS += -$(ARCH) CC='$(CC)' LD='$(CC)' CXX='$(CXX)' \
CFLAGS='$$CFLAGS $(STD_CFLAGS) -Wall' \
CXXFLAGS='$$CFLAGS $(STD_CFLAGS) -Wall' \
LDFLAGS='$$LDFLAGS $(STD_LDFLAGS)'

mexsrc := $(wildcard *.c)
mextgt := $(mexsrc:.c=.$(MEXEXT))

.PHONY: all
all: $(mextgt)

%.$(MEXEXT) : %.c
	$(MEX) $(MEXFLAGS) $< -output $@

vl_fconv.$(MEXEXT) : MEXFLAGS += -lmwblas

.PHONY: info
info:
	@echo MEX           = '$(MEX)'
	@echo MATLABROOT    = '$(MATLABROOT)'
	@echo MEXEXT        = '$(MEXEXT)'
	@echo MEXFLAGS      = '$(MEXFLAGS)'
	@echo mexsrc        = $(mexsrc)
	@echo mextgt        = $(mextgt)
	@echo CC            = $(CC)
	@echo CXX           = $(CXX)
	@echo SDKROOT       = $(SDKROOT)

.PHONY: clean
clean: distclean
	rm -f `find . -name '*~'`

.PHONY: distclean
distclean:
	rm -f $(mextgt)
